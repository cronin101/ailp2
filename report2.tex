%
%  outline latex source document for AILP assignment 1.
%  use pdflatex to format this.
%
\documentclass[11pt,a4paper]{article}
\usepackage{geometry}
\usepackage{amssymb,amsmath}            % if some maths is needed
\usepackage{graphicx}                   % if any images are to be included
% pick a different font if desired

\begin{document}
\begin{titlepage}
    \centering
    \vspace*{10pt}
    \large
    \bfseries
    AILP Assignment 2 \\
    \vfill
    Handwriting Recognition\\
    \normalfont
    \small
    Using on-line and off-line methods\\
    \vfill
    Aaron Cronin\\
    s0925570\\
    \textbf{\today}\\
\end{titlepage}
\tableofcontents
\pagebreak
\section{Introduction}

Previously I worked to improve the performance of an off-line handwriting recognition system. I did so by implementing preprocessing algorithms to normalise arrays of points being read into an existing recognition system. This time I have implemented a series of classification systems that will perform the entire procedure of reading inputs from a testing set and classifying them using data from a reference set.
\section{Aims and hypothesis}
Unlike the last assignment, for this assignment I will use none of the supplied code. This will force me to understand and design every step that is required when creating a handwriting recognition system. However, the benefit of this will be that all code will be written with a familiar style and I will have greater flexibility about how I choose to implement the required processes.
My aim is to produce a working classification system that uses a combination of off-line and on-line methods in order to classify handwriting samples with a high degree of accuracy but with reasonable performance.
\section{Overview my implementation}
After inefficiently trialling some basic ideas in \emph{Ruby}, the first thing that I wrote was the \emph{computedistance.c} file for finding the distance between two sets of points, the method of time-scale variation that I chose was \emph{Dynamic Time Warping}. After reading some of the recommended material on the course webpage, it came clear that this was a canonical implementation of the dynamic programming algorithm that I had previously studied in another course. 
After designing a program that would load files and compare their distance taking commandline arguments as control, I started calling this with various parameters from within a controlling \emph{Ruby} script with a loop available to iterate through the directories of the reference set.
I then expanded this with classifaction methods and finally reducing of the search space to improve speed.
\section{Improving accuracy}
The method that I used for classifying the data works by attempting to classify a given sample by comparing it to the reference set of samples over all people and classes, obviously this is an incredibly computationally expensive approach as it had to perform \emph{Dynamic Time Warping} to each sample in the reference set with respect to an input. However I found that this repeated attempt to match classifications against known values lead to a consistent level of high accuracy when classifying inputs. In addition it was adept at providing a classification for an unusual writing style that would be lost when averaging the reference set, as long as at least one example of the unusual style had been seen before. After I had settled on the idea of ranking matches to existing samples using \emph{DTA}, the time came for me to implement how to choose a final classification given this ranking.
\subsection{Testing classification methods}
Classifiers would be fed a sorted list of all possible classifications for the input sample. The keys in this list were the ids of the corresponding reference that the sample had been compared to, and it's classification. The values were the corresponding distance as outputted by the \emph{Dynamic Time Warping} algorithm.
I implemented two seperate classification methods:
\begin{description}
  \item[Best matching class] The top element from this sorted list would be chosen. This element would represent the singular sample in the reference database that matched closest to the input, it's class would then be returned as a classification. One way of describing this approach would be \emph{``If it looks most like an 'H' that I saw, I think it is an 'H'''}
  \item[Most commonly matching class] A predefined number of elements would be taken from the top of this sorted list, these elements represent the best \emph{n} matches. The number of occurrences of each class within these matches would then be counted and the class that occured the most times would be returned. The corresponding explaination for this approach would be \emph{``Of the 10 things that I have seen that most look like this, most of them are 'H's''} In the case that there was not a distict majority winner, I defaulted to the classifaction method above and just returned the best match.
\end{description}
\paragraph{Results} Surprisingly I found that the \emph{Best matching class} approach consistently dominated the \emph{Most commonly matching class} approach regardless of the number of elements I decided to take from the top of the sorted list (apart from one of course, as this would make the two approaches functionally identical). I was expecting the second algorithm to be more reliable as it took into account a greater number of matches, however it seems that there was enough variation within the dataset that sometimes there were not enough good matches within the top selection and yet the top result was a very well matched previous example of that writing style.
\begin{center}
\begin{tabular}{ | c | c | c | }
  \hline
    Method & Result on digits & Result on uppercase \\ \hline
    Best Match & $90.88\%$ & $86.01\%$ \\ \hline
    Best Match(es) & $85.88\%$ & $83.89\%$ \\
      \hline
\end{tabular}\\
Best matches used the top 10 results, empirically tested to appear be the best.
\end{center}
In contrast the results of unpreprocessed off-line distance comparison were: $66.58\%$ for digits and $56.63\%$ for uppercase characters. 
\section{Preprocessing algorithms}
Since the design of my code was so disjoint from the previous project, it was impossible to reuse directly any of my existing preprocessing solutions. This wasn't all bad though, as my incredibly high number of iterations using the basic \emph{DTW} algorithm relied on a cut-down approach; too many preprocessing steps would increase the time taken by each of these iterations.
It was for the reasons of simplicity and efficiency that I chose to implement only the best preprocessing algorithm from my previous coursework, \emph{Centroid Normalisation}.
The benefits of \emph{Centroid Normalisation} were great:
\begin{center}
\begin{tabular}{ | c | c | c | }
  \hline
    Method & Result on digits & Result on uppercase \\ \hline
    Best Match with \emph{Centroid Normalisation} & $97.09\%$ & $94.55\%$\\ \hline
\end{tabular}\\
\end{center}
It was at this point that I decided that the system had reached it's potential as an 'good' accuracy classifier and I moved into improving the performance of the system.
\section{Improving performance}
\subsection{Methods}
There is a tradeoff between accuracy of results and computation required when classifying handwriting, this is due to the computationally expensive nature of comparing two sets of points to the full extent. When coupled with the larger number of possible classifications and large dataset, this can cause classification to take an unreasonable length of time. My solution to this was to abandon any provided code and start fresh applying efficient programming practices to maximimise the throughput of my classification. I wrote the entirety of the distance calculation code in \emph{C}, both for off-line and on-line classifications, avoiding any unecessary code that could adversely effect performance. The code is executed using \emph{Ruby} scripts and is dual threaded when calling the compiled \emph{C} code to nearly double the speed of execution. The reason for using \emph{Ruby} despite aiming primarily for speed was to enable rapid prototyping of ideas when designing the majority of my controlling code structure. A significant improvement, especially when reducing the overheads of the testing script itself, could be gained from rewriting the current scripting language into a compiled language. Obviously this approach would be taken if the implementation were to be transfered to an actual device and used to fulfil it's specification, however time constraints encountered due to the nature of the extra workload encountered without using existing code prevented me from being able to carry this out.
\subsection{Why bother?}
The reason for this optimisation is so that I could implement an algorithm that would have a high success rate due to an extensive search for an optimal classification. Without optimisation this could cause an unreasonable duration required to clasify a result. However, despite efforts to maximise the efficiency of core computational algorithms; my main focus when writing the classier was to maintain the highest amount of accuracy possible and then see if any cheap gains in speed could be achieved. I feel this is justifiable by the ever-growing increases in mobile device speed allowing more and more computationally expensive implementations that were historically relegated to bulkier workstations being implemented into simple devices.
\subsection{Reducing the search space}
Whilst the accuracy of off-line methods isn't particularly impressive compared to the more iteration-intensive on-line methods, they are much faster due to only reference patterns being inspected instead of every instance of reference arrays.
This made using off-line methods initially and then restricting the areas to on-line search a viable method of increasing the efficiency of the classifier.
\paragraph{Method} I chose to run an off-line classification of the sample presented before any other methods, and then ranked classes by the off-line distance results. I then restricted the search to only the best half of this sorted list, halving the number of calls to the \emph{DTW} program.
However due to the inaccuracy of the basic off-line ranking I used without any effective preprocessing, this impacted the performance of the hybrid algorithm slightly. With more time I believe that I could increase the accuracy of the off-line ranking sufficiently more so that this method of speeding up the final classification significantly would have an almost negligible effect on the accuracy. 
\section{Results of testing}
\begin{center}
  With sample providers 1-80 as the testing set and 81-159 as the reference set
  \begin{tabular}{ | c | c | c | c | c | }
  \hline
    Method & Result on d & Result on u & Time to classify on d & Time to classify on u \\ \hline
    Best Match  & $95.75\%$ & $91.25\%$ & 0.59s & 1.39s\\ \hline
    Hybrid & $91.75\%$ & $90.63\%$   & 0.29s & 0.75s \\ \hline
    Offline & $65.5\%$ & $54.71\%$ & 0.03s & 0.05s \\ \hline
  \end{tabular}\\
\vfill
  K-fold cross-validation with testing set and reference set swapped.
\begin{tabular}{ | c | c | c | c | c | }
  \hline
    Method & Result on d & Result on u & Time to classify on d & Time to classify on u \\ \hline
    Best Match & $97.09\%$ & $94.55\%$ & 0.59s & 1.39s \\\hline
    Hybrid & $93.41\%$ & $92.45\%$   & 0.29s & 0.75s \\ \hline
    Offline & $66.45\%$ & $56.43\%$ & 0.03s & 0.05s \\ \hline
  \end{tabular}\\
\vfill
Average of both
\begin{tabular}{ | c | c | c | c | c | }
  \hline
    Method & Result on d & Result on u & Time to classify on d & Time to classify on u \\ \hline
    Best Match & $96.42\%$ & $92.9\%$ & 0.59s & 1.39s \\\hline
    Hybrid & $92.58\%$ & $91.6\%$   & 0.4s & 0.75s \\ \hline
    Offline & $65.98\%$ & $55.57\%$ & 0.03s & 0.05s \\ \hline
  \end{tabular}\\

\end{center}
\paragraph{Maximum accuracy}
The results show that a very high level of accuracy can be obtained with an exaustive trial of on-line classification, this comes at the cost of a computationally intensive classification process. However, in the case of digits, although it took a very long time to obtain the results for ~800 unique inputs, the time taken to classify each individual was not too unreasonable. For example the exaustive closest-match classification of digits took only 0.59 seconds per classification. This is not too unreasonable if performed at the same time as input, for example a user could be writing on a tablet and the characters they have written could be replaced in real time by digit output as long as they were writing less than 101 characters per minute. When performing exaustive classification on uppercase characters, the increased number of possible classifications increases the time taken by the classifier linearly. The character classification now takes 1.39 seconds per classification and this limits the user to being able to enter 42 characters, or on average 8 words per minute in order for real-time classification and display to be possible.
\paragraph{Increased performance}
When running the off-line optimised program, with a reduced search space, the accuracy of the digit classifier drops to $92.58\%$ due to the inaccuracies of the off-line methods which I failed to perfect. However the time taken for each classification drops to 0.29 seconds, This is quite significant, being 100\% faster, if higher performance is needed; as 206 characters a minute can now be inputted in real-time classification. This advantage is even more pronounced when classifying the uppercase characters with more possible classifications. The accuracy remains at a respectable $91.6\%$ whilst the time taken to classify each character has dropped to a noticably faster 0.75s. This allows an extra 36 characters per minute which is a noticable improvement. A total of 80 characters per minute is enough for casual note taking. 
Note that majority of the code could easily be adapted to run on more processor cores, the test laptop is only a dual-core but with four cores the performance of the algorithm could be almost doubled with a simple tweaking of the \emph{Thread.new} arrangement inside the \emph{Ruby} scripts, and a function that would wait until all threads have completed.
\section{Conclusion}
I have presented an implementation of on-line classification with off-line optimisation that exceeds a 90\% success rate whilst still being usable on today's mobile hardware.
I generated telemetric data and confusion matrices for every combination of classification method and dataset in order to show that my programs were behaving as intended and so that I could objectively measure their performance.
I have learned that applying a cheap but inaccurate solution, in part, to an accurate but complex one can provide the best of both worlds in the right scenario.
\end{document}
