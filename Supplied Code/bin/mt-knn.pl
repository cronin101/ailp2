#! /usr/bin/perl
#	$Id: mt-knn.pl,v 1.1 2007/10/20 11:10:29 sim Exp sim $
#	by Hiroshi Shimodaira <h.shimodaira@ed.ac.uk>   4/October/2007
#
#@	Usage: mt-knn.pl [-k num] [recogntion_output_with_multiple_templates]
#@	Options:
#@	 -k num: set the value of "k" of k-NN to num.  [k=1 by default]
#
# Description: This program carries out k-NN decision against the output 
# of recognition (distance calculation) with multiple templates.
# The output of this program can be fed to cv-ana-test.sh to obtain
# a confusion-matrix and correct recognition rate.
#
# e.g. % ./mk-knn.pl < OUTPUT | ./cv-ana-test.sh
#   (where OUTPUT is a name of the file in which you store the output of
#    recognition (distance calculation) program.
#
# Input Format Specifications:
#  The format is almost the same with the output format of "run-pixel-dist.sh" 
#  when the number of references is multiplied by a factor of the number of
#  references per class.
#  In addition to the header "#>> Class= n" which is also used in the output of
#  run-pixel-dist.sh, another special header is required for this script 
#  to work properly, which is
#  "#>> MULTIPAT= n"
#  where 'n' in the right hand side of '=' stands for the number of 
#  reference patterns per class. This declaration should appear only once
#  and it should appear before any recognition output.
#  This declaration is needed every time when correct class number changes.
#  Other lines beginning with '#' are ignored and just fed to the output.
#
#  An example of input where each class has two templates is shown as follows:
#   #>> MULTIPAT=2
#   #>> CLASS=0
#   0 0     287.075		# D(x0, R0)
#   0 1     251.49      	# D(x0, R1)
#   0 2     1696.55     	# D(x0, R2)
#   ....    .....
#   0 19    1749        	# D(x0, R19))
#
#   1 0     183.024     	# D(x1, R0)
#   1 1     249.611     	# D(x1, R1)
#   ....    .....
#  where each line (except for lines beginning with '#') consists of
#  three fields:
#    Field 1: input data number (starting at 0)
#    Field 2: reference number. In the above example, R0 and R1 belong to
#             Class 0, R2 and R3 belong to class 1, and so on. There are
#	      20 references in total if the number of classes is 10 and 
#	      the number of references per class is 2.
#    Field 3: distance
#

($IAM = $0) =~ s/.*\///;

$knn = 1;	# the value of k for k-NN

#---------------------------------------------------------------------

while( $ARGV[0] =~ /^-/ ){
    $_ = shift;
    if( /^-help/ ){
	exec("Usage $0");
    }elsif( /^-k*/ ){
       $knn = shift;
    }elsif( /^-verbose/){
	$verbose++;
    }else{
      die("unknown option: $_\n");
   }
}
push(@ARGV, "-") if( $#ARGV < 0);

printf("#>> script= $IAM\n");
printf("#>> kNN= %d\n", $knn);

$nr = 0; $cidb4 = -1; $nrefpc = 1;

while(<>){
    if( /^\#>> CLASS/ ){	# definition of the true class number
       print;
       next;
    }elsif( /^\#>> MULTIPAT=\s*(\d+)/ ){
       print;
#       ($hd1, $hd2, $nrefpc) = split;       # number of templates per class
	$nrefpc = $1;
       next
    }elsif( /^\#/ ){
       print;
       next;
    }
    chop;
    if( /^[ \t]*$/ ){	# blank line: end of recognition of an input data
       $max_cid = $cid;
       evaluate($num) if($nr);
       $nr = 0;  $cidb4 = -1;
       print("\n");
       next;
    }
    ($num, $rid, $d) = split;
    $ds[$nr] = $d;
    $cid = int( $rid / $nrefpc );	# get the class id of the reference pattern.
    $cs[$nr] = $cid;
    $nr++;

#    printf("# %2s %2s(%d):  %g\n", $num, $rid, $cid, $d);
}

sub evaluate {
   local($data_id) = @_;
   my(@os, $k, $i, @nn, $idx);
   my(@cmindist, @ct, $cfinal, $d);
   
   @os = 0..($nr-1);
   @nn = sort(DSsort @os);	# indices for nearest neighbours

   $i = 0;
   foreach $k (@nn){
#      printf("# %d %d (%d)\t%g\n", $i, $k, $cs[$k], $ds[$k]);
      $i++;
   }
   #--- voting: count the number of data in k-NN for each class ---
   for($i = 0; $i <= $max_cid; $i++){
      $ccount[$i] = 0;
      $cmindist[$i] = 99999;	# ugly coding...
   }
   for($k = 0; $k < $knn; $k++){
      $idx = $nn[$k];	     # index to the data
      $cid = $cs[ $idx ];
      $ccount[$cid]++;
      #-- memorise the data which gives the shortest distance in the class
      $cmindist[$cid] = $ds[$idx] if($cmindist[$cid] > $ds[$idx]);
#      printf("#-- %d-NN: cid=$cid idx=$idx  dmin=%g\n", $k+1, $cmindist[$cid]);
   }
   #--- 
   @ct = sort(CCsort (0..$max_cid));
   # in case of a draw
   if($knn > 1 && ($ccount[$ct[0]] == $ccount[$ct[1]]) ){
      $cfinal = ($cmindist[$ct[0]] < $cmindist[$ct[1]])? $ct[0] : $ct[1];
   }else{
      $cfinal = $ct[0];
   }
#   printf("#:: ccount=%s, ct=%s\n", "@ccount", "@ct");
#   printf("#---> cfinal= $cfinal count=%d\n", $ccount[$ct[0]]);

   for($i = 0; $i <= $max_cid; $i++){
      $d = ($i == $cfinal)? 0.0 : 1.0;
      printf("%d %d\t%g\n", $data_id, $i, $d);
   }
}

sub DSsort {
   $ds[$a] <=> $ds[$b];
}

sub CCsort {
   $ccount[$b] <=> $ccount[$a];
}
