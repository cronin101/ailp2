#!/bin/sh
# dtw.sh  -  wrapper code for java DTW class
AILP_HOME="`dirname $0`/../"
export CLASSPATH=../src:../src/hwr:$AILP_HOME/src:$AILP_HOME/src/hwr:.

java LTWDist $*
