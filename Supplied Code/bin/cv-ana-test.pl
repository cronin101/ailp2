#! /usr/bin/perl
#	$Id$
#	by Hiroshi Shimodaira <h.shimodaira@ed.ac.uk>   1/November/2005
#
# Description: This program reads the output of recogniser and gives
# correct recognition rate and a confusion matrix.
#
# Usage:   % ./cv-ana-test.pl < OUTPUT
#
# Input format Specifications: a typical example for a digit recognition
# looks like this:
#    #>> CLASS= 0
#    0 0     287.075		# D(x0, R0)
#    0 1     1696.55		# D(x0, R1)
#    0 2     1434.86		# D(x0, R2)
#    ...     .......           ...........
#    0 9     1616.16		# D(x0, R9)
#  
#    1 0     183.024		# D(x1, R0)
#    1 1     342.353		# D(x1, R1)
#    ...     .......            ...........
#    ...     .......            ...........
#    #>> CLASS= 1
#    0 0     2130.57		# D(x0, R0)  NB: this x0 differs from x0 above
#    0 1     64.5767		# D(x0, R1)
#    0 2     525.088		# D(x0, R2)
#    ...
# where the first line "#>> CLASS= 0" tells that the following lines are the
# recognition output whose correct class number is 0.
# Next each line consists of three fields:
#    Field 1: input data number (starting at 0)
#    Field 2: reference number. 
#    Field 3: distance
# A blank line is needed as a separator between recognition sessions of 
# different input data. 
# Other lines beginning with '#' are comment lines.

# $home = $ENV{"HOME"};
# unshift(@INC, "$home/lib/Perl");

# require("common");
package main;

#---------------------------------------------------------------------

while( $ARGV[0] =~ /^-/ ){
    $_ = shift;
    if( /^-help/ ){
	exec("Usage $0");
    }elsif( /^-verbose/){
	$verbose++;
    }else{
      die("unknown option: $_\n");
   }
}
push(@ARGV, "-") if( $#ARGV < 0);

while(<>){
    if( /^\#>> CLASS/ ){	# definition of the true class number
	($hd1, $hd2, $true_class) = split;
	$true_class++;	# change the counting origin from 0 to 1.
	$classes{ $true_class }++;
    }elsif( /^\#/ ){
	next;
    }
    chop;
    if( /^[ \t]*$/ ){	# blank line: enf of recognition of an input data
	evaluate() if( $nr > 0 );
	next;
    }
    ($num, $cid, $d) = split;
#    printf("# %2s %2s:  %g\n", $num, $cid, $d);
    
    $num++; $cid++;	# change the counting origin from 0 to 1.
    $ds{$num,$cid} = $d;
    $nr++;
    $cid_max = $cid if($cid > $cid_max);
}

evaluate() if( $nr > 0 );

show_summary();
printf("\n");
show_confusion_matrix($cid_max, *conf);

sub evaluate {
    my($rank, $key, $num, $cid);

    $true_counts[$true_class]++;

    $rank = 1;
    foreach $key (sort(Mysort keys(%ds)) ){
	($num, $cid) = split(/$;/, $key);
	printf("%3d: %2s %2s:  %g\n", $rank, $num, $cid, $ds{$key}) if($verbose> 100);
	if($rank == 1){
	    printf("%d: %d -> %d", $count+1, $true_class, $cid) if($verbose> 0);
	    $conf{$true_class, $cid}++;
	    if($cid == $true_class){
		$correct_counts[$true_class]++;
	    }
	    $count++;
	}
	$rank++;
    }
    print("\n") if($verbose);

    #--- reset variables ---
    undef(%ds);
    $nr = 0;
}

sub show_summary {
    my($cid);

    $ntrue = 0; $ncorrect = 0;
    foreach $cid (sort(srt_by_inc keys(%classes))){
	printf("cid %2d   %d / %d = %.1f \%%\n", $cid, $correct_counts[ $cid ], $true_counts[$cid], $correct_counts[ $cid ] / $true_counts[$cid] * 100.0);
	$ntrue += $true_counts[$cid];
	$ncorrect += $correct_counts[$cid];
    }
    printf("Total:  %d / %d = %.2f %%\n", $ncorrect, $ntrue, $ncorrect / $ntrue * 100.0);
}

sub show_confusion_matrix {
    local($nc, *conf) = @_;
    my($cin, $cout, $sum);

    printf("I\\O", $cin);
    for($cout = 1; $cout <= $nc; $cout++){
	printf("   %3d",$cout);
    }
    printf("\n");

    for($cin = 1; $cin <= $nc; $cin++){
	$sum = 0;
	for($cout = 1; $cout <= $nc; $cout++){
	    $sum += $conf{$cin, $cout};
	}
	printf("%3d", $cin);
	for($cout = 1; $cout <= $nc; $cout++){
	    printf(" %5.1f", $conf{$cin, $cout} / $sum * 100.0);
	}
	printf("\n");
    }
}

sub Mysort {
    $ds{$a} <=> $ds{$b}
}

sub srt_by_inc {
   $a <=> $b;
}
