#! /usr/bin/python -W ignore
# vi:ts=4:tw=78:shiftwidth=4:expandtab
# vim600:fdm=marker
#
# disp_ascii.py  -  display pen data in ascii matrix format, require matplotlib and nunmpy installed
# usage: disp_ascii.py  input --save figure.png
 
import sys
import warnings
from optparse import OptionParser
from numpy import *
from pylab import *

def main ():

    # parsing options{{{
    # the default value, if not specified by 'default=xxx', is None 
    usage = "usage: %prog [options] input output"
    parser = OptionParser(usage)
    parser.add_option("-s", "--save", type="string", help="save image to a file (png or ps only)")
    (options, args) = parser.parse_args()
    #}}}

    # rcParams['toolbar'] = None
    input = sys.stdin

    if len(args) > 0:
        input = open(args[0])

    lines = [s.strip() for s in input]
    s = lines[0]
    a,w,h = s.split()
    w = int(w.split('=')[1])
    h = int(h.split('=')[1])
    print 'width=%d, height=%d' % (w,h)
    m = zeros([w,h], float)
    for i in range(h):
        s = lines[2+i]
        m[i,:] = array([float(x) for x in s.split()])
    imshow(m, cmap=cm.binary,interpolation='nearest')
    if options.save:
        print 'saving to %s...' % options.save
        savefig(options.save)
    #grid(True)
    show()

with warnings.catch_warnings():
    warnings.simplefilter("ignore")


if __name__ == "__main__":
    main()

