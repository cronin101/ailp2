#! /bin/sh

RECOG="../bin/dtw.sh"

#---- for upper case alphabet---
# KIND="u"	# character set:
# NCLASSES=26	# number of classes
#----- for digits --------------
KIND="d"	# character set:
NCLASSES=10	# number of classes

DATADIR="/group/teaching/ailp/DB/mit-points"
# REFDIR="REFSEG-${KIND}${NCLASSES}-cv2-1"
REFDIR="${DATADIR}/s014"
LISTFILES="../CV-LISTS/list-cv2-1"	# filename containing a list of writers used for test

WIDS=`cat ${LISTFILES} | tr "\n" " "`

NWRITERS=`echo $WIDS|wc -w| sed "s/[ \t]//g"`

echo "# REFDIR= $REFDIR"
echo "# NWRITERS= $NWRITERS"

N=1
while [ $N -le "$NCLASSES" ]
do
  S=`printf "%02d" $N`
  CLASSES="$CLASSES ${KIND}$S"
  N=`expr $N + 1`
done

echo "# CLASSES= $CLASSES"

CID=0
for CLASS in $CLASSES
do
  echo "#>> CLASS= $CID"
  {
      rm -f input.list
      for WID in $WIDS
	do
	echo $DATADIR/${WID}/${WID}${CLASS}.dat >> input.list
      done
  }  
  rm -f ref.list
  for f in `ls $REFDIR/s???${KIND}??.dat`; do
      echo $f >> ref.list
  done
  $RECOG input.list ref.list

  CID=`expr $CID + 1`
done

rm -f input.list ref.list


