import java.lang.*;
import java.io.*;
import java.util.*;
import hwr.*;

public class PixelDist {
    public static void main(String[] args) {

        if (args.length != 4) {
            System.out.println("Compute pixel distances between files" +
                               " in input_list and ref-list");
            System.out.println("usage: width height input_list ref_list");
            System.exit(-1);
        }


        int width = Integer.parseInt(args[0]);
        int height = Integer.parseInt(args[1]);
        PixelDataReader input_reader =
	    new PixelDataReader(width, height);
        PixelDataReader ref_reader = new PixelDataReader(width, height);
        float input_data[][][] = input_reader.readFileList(args[2]);
        float ref_data[][][] = ref_reader.readFileList(args[3]);


        for (int i = 0; i < input_data.length; ++i) {
            for (int j = 0; j < ref_data.length; ++j) {
                System.out.println(i + " " + j + "\t" +
				   computeDist(input_data[i], ref_data[j]));
            }
	        System.out.println("");
        }
        System.out.println("");
    }

    static float computeDist(float a[][], float b[][]) {
        int width = a.length;
        int height = a[0].length;
        float sum = 0;
        for (int i = 0; i < width; ++i) {
            for (int j = 0; j < height; ++j) {
                sum += (a[i][j]-b[i][j])*(a[i][j]-b[i][j]);
            }
        }
        return sum/(width*height);
    }
}
