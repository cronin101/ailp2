import java.lang.*;
import java.io.*;
import java.util.*;
import hwr.*;

public class DTWDist {
    public static void main(String[] args) {

        if (args.length != 2) {
            System.out.println("Perform dynamic time warping alignment on reference patterns");
            System.out.println("usage: input_list ref_list");
            System.out.println("output format: input_id ref_id distance");
            System.exit(-1);
        }

        TimeWarping warpFun = new DynamicTimeWarping(new SquaredEuclidDistance());
        float input_pats[][][];
        float ref_pats[][][] = new float[args.length-1][][];
        PenDataReader reader = new PenDataReader(true);
        input_pats = reader.readFileList(args[0]);
        ref_pats = reader.readFileList(args[1]);

        for (int i = 0; i < input_pats.length; ++i) {
            for (int j = 0; j < ref_pats.length; ++j) {
                System.out.println(i + " " + j + "\t" + warpFun.calcDistance(input_pats[i], ref_pats[j]));
            }
            System.out.println();
        }
        System.out.println();
    }
}
