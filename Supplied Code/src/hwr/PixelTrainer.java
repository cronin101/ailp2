import java.lang.*;
import java.io.*;
import java.util.*;
import hwr.*;

public class PixelTrainer {
    public static void main(String[] args) {

        if (args.length != 3) {
            System.out.println("Compute averaged pixel values from a list of image files");
            System.out.println("usage: width height input_list");
            System.exit(-1);
        }

        int width = Integer.parseInt(args[0]);
        int height = Integer.parseInt(args[1]);
        PixelDataReader reader = new PixelDataReader(width, height); //, true);
        float input_data[][][] = reader.readFileList(args[2]);
        float avg_data[][] = computeAveragePixel(input_data);
        printPixelData(avg_data);
    }

    static float [][] computeAveragePixel(float data[][][]) {
        int ndata = data.length;
        int width = data[0].length;
        int height = data[0][0].length;
        float a[][] = new float[width][height];

        for (int i = 0; i < width; ++i)
            for (int j = 0; j < height; ++j)
                a[i][j] = 0;

        for (int n = 0; n < ndata; ++n) {
            float b[][] = data[n];
            for (int i = 0; i < width; ++i)
                for (int j = 0; j < height; ++j)
                    a[i][j] += b[i][j];
        }

        for (int i = 0; i < width; ++i)
            for (int j = 0; j < height; ++j)
                a[i][j] /= (float)ndata;

        return a;
    }

    static void printPixelData(float a[][]) {
        int width = a.length;
        int height = a[0].length;
        System.out.println("> WIDTH " + width + " HEIGHT " + height);
        System.out.println("> x[][]");
        for (int i = 0; i < width; ++i) {
            for (int j = 0; j < height; ++j) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println("");
        }
    }
}
