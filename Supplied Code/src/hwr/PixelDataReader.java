package hwr;

import java.lang.*;
import java.io.*;
import java.util.*;

public class PixelDataReader {
    int width;
    int height;
    //    boolean ascii;

    public PixelDataReader(int width, int height) { // , boolean ascii) {
        this.width = width;
        this.height = height;
	//        this.ascii = ascii;
    }

    public float[][] readFile(String file) {
        float a[][] = new float[width][height];
	// if (ascii) {
            try {
                BufferedReader input = new BufferedReader(new FileReader(file));
                String line;
                int row = 0;
                int col = 0;
                while ((line = input.readLine()) != null) {
                    if (line.length() == 0 || line.charAt(0) == '>')
                        continue;

                    StringTokenizer st = new StringTokenizer(line);
                    while (st.hasMoreTokens()) {
                        a[row][col] = Float.parseFloat(st.nextToken());
                        if (++col >= width) {
                            col = 0;
                            ++row;
                        }

                    }
                }
                if (row != height) {
                    System.err.println("Error when loading " + file +
				       ": wrong width/height value");
                    throw new IOException("Error when loading " + file +
					  ": wrong width/height value");
                }
                input.close();
            } catch (IOException ex) {
                System.err.println("Error when loading " + file);
            }
	// }   else {
        //     try {
        //         DataInputStream input =
    	// 	    new DataInputStream(new FileInputStream(file));
        //         int nbytes = width*height/8;
        //         int row = height-1;
        //         int col = 0;
        //         for (int i = 0; i < nbytes; ++i) {
        //             byte b = input.readByte();
        //             for (int j = 0; j < 8; ++j) {
        //                 a[row][col] = ((b & 0200) > 0 ? 1 : 0);
        //                 b <<= 1;
        //                 if( ++col >= width ){
        //                     --row;  
        //                     col = 0;
        //                 }
        //             }
        //         }
        //         input.close();
        //     } catch (IOException ex) {
        //         System.err.println("Error when loading " + file);
        //     }
	// }
        return a;
    }

    public float[][][] readFileList(String filelist) {
        Vector v = new Vector();

        try {
            BufferedReader input = new BufferedReader(new FileReader(filelist));
            String file;
            while ((file = input.readLine()) != null) {
                file = file.trim();
                if (file.length() > 0) 
                    v.addElement(readFile(file));
            }
            input.close();
        } catch (IOException ex) {
            System.err.println("Error when loading from filelist:" + filelist);
        }
        // convert vector into array
        float a[][][] = new float[v.size()][][];
        for (int i = 0; i < v.size(); ++i) {
            a[i] = (float[][])v.get(i);
        }
        return a;
    }
}

