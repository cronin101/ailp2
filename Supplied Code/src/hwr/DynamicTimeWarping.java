package hwr;

/* see http://en.wikipedia.org/wiki/Dynamic_time_warping */
public class DynamicTimeWarping extends TimeWarping {
    public DynamicTimeWarping(Distance dist) {
        super(dist);
    }

    public float calcDistance(float a[][], float b[][]) {
        int ni = a.length;
        int nj = b.length;
        float dtw[][] = new float[ni][nj];

        dtw[0][0] = 0;

        for (int i = 0; i < ni; ++i) {
            for (int j = 0; j < nj; ++j) {
                float cost = distFun.dist(a[i], b[j]);
                dtw[i][j] = min3(getdtw(dtw, i-1, j) + cost, 
                        getdtw(dtw, i, j-1) + cost, 
                        getdtw(dtw, i-1, j-1) + cost*2);
            }
        }
        return dtw[ni-1][nj-1]/(ni+nj);
    }

    float min3(float a, float b, float c) {
        return Math.min(Math.min(a, b), c);
    }

    float getdtw(float dtw[][], int i, int j) {
        if (i == -1 && j == -1)
            return 0;
        else if (i < 0 || j < 0)
            return Float.MAX_VALUE;
        else
            return dtw[i][j];
    }
}
