package hwr;

public class SquaredEuclidDistance extends Distance {
    float dist(float a[], float b[]) {
        assert a.length == b.length;

        float sum = 0;
        for (int i = 0; i < a.length; ++i) {
            sum += (a[i]-b[i])*(a[i]-b[i]);
        }
        return sum;
    }
}
