package hwr;

import java.lang.*;
import java.io.*;
import java.util.*;

public class PenDataReader {
    boolean ignorePenUp;

    public PenDataReader(boolean ignorePenUp) {
        this.ignorePenUp = ignorePenUp;
    }

    public float[][] readFile(String file) {
        Vector v = new Vector();

        try {
            BufferedReader input = new BufferedReader(new FileReader(file));
            String line;
            while ((line = input.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(line);
                while (st.hasMoreTokens()) {
                    float d[] = new float[2];
                    d[0] = Float.parseFloat(st.nextToken());
                    d[1] = Float.parseFloat(st.nextToken());
                    int id = Integer.parseInt(st.nextToken()); 
                    if (st.hasMoreTokens()) {
                        int time = Integer.parseInt(st.nextToken());
                    }
                    if (id == 0 && ignorePenUp)
                        continue;
                    else {
                        v.addElement(d);
                    }
                }
            }
            input.close();
        } catch (IOException ex) {
            System.err.println("Error when loading " + file);
        }
        // convert vector into array
        float a[][] = new float[v.size()][2];
        for (int i = 0; i < v.size(); ++i) {
            float d[] = (float[])v.get(i);
            a[i][0] = d[0];
            a[i][1] = d[1];
        }
        return a;
    }

    public float[][] readStream(InputStream in) {
        Vector v = new Vector();

        try {
            BufferedReader input = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = input.readLine()) != null && line.trim().length() > 0) {
                StringTokenizer st = new StringTokenizer(line);
                while (st.hasMoreTokens()) {
                    float d[] = new float[2];
                    d[0] = Float.parseFloat(st.nextToken());
                    d[1] = Float.parseFloat(st.nextToken());
                    int id = Integer.parseInt(st.nextToken()); 
                    if (id == 0 && ignorePenUp)
                        continue;
                    else {
                        v.addElement(d);
                    }
                }
            }
            if (v.isEmpty()) {
                if (line != null) {
                    float a[][] = new float[0][0];
                    return a;
                } else
                    return null;
            }
        } catch (IOException ex) {
            System.err.println("Error when loading from InputStream");
            //return null;
        }
        // convert vector into array
        float a[][] = new float[v.size()][2];
        for (int i = 0; i < v.size(); ++i) {
            float d[] = (float[])v.get(i);
            a[i][0] = d[0];
            a[i][1] = d[1];
        }
        return a;
    }

    public float[][][] readFileList(String filelist) {
        Vector v = new Vector();

        try {
            BufferedReader input = new BufferedReader(new FileReader(filelist));
            String file;
            while ((file = input.readLine()) != null) {
                file = file.trim();
                if (file.length() > 0) 
                    v.addElement(readFile(file));
            }
            input.close();
        } catch (IOException ex) {
            System.err.println("Error when loading from filelist:" + filelist);
        }
        // convert vector into array
        float a[][][] = new float[v.size()][][];
        for (int i = 0; i < v.size(); ++i) {
            a[i] = (float[][])v.get(i);
        }
        return a;
    }
}
