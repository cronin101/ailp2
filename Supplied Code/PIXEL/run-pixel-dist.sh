#! /bin/bash

AILP_HOME="`dirname $0`/../"
RECOG="$AILP_HOME/bin/pixel_dist.sh"		# recognition program name

#---- for upper case alphabet---
# KIND="u"	# character set:
# NCLASSES=26	# number of classes
#----- for digits --------------
KIND="d"	# character set:
NCLASSES=10	# number of classes

# size of pixel
PIXEL_WIDTH=64

# Directory of PIXEL data
DATADIR=/group/teaching/ailp/DB/mit-pixels-${PIXEL_WIDTH}

# Directory of reference patterns
REFDIR="REF${PIXEL_WIDTH}-${KIND}${NCLASSES}-cv2-0"

# filename containing a list of writers used for test
LISTFILES="$AILP_HOME/CV-LISTS/list-cv2-1"

# include the list of writers into the variable
WIDS=`cat ${LISTFILES} | tr "\n" " "`

# calculate the number of writers in the list, and assign it to the variable
NWRITERS=`echo $WIDS|wc -w| sed "s/[ \t]//g"`

echo "# REFDIR= $REFDIR"
echo "# NWRITERS= $NWRITERS"

#--- make a list of classes ---
N=1
while [ $N -le "$NCLASSES" ]
do
  S=`printf "%02d" $N`
  CLASSES="$CLASSES ${KIND}$S"
  N=`expr $N + 1`
done
#

echo "# CLASSES= $CLASSES"

#--- make a list of reference pattern files; ---
#--- if there is no reference directory, quit. ---
if ! [ -d ${REFDIR} ]; then
    echo Error: no reference directory
    exit 1
fi	

rm -f ref.list
for f in `echo $REFDIR/ref*`; do
		  echo $f >> ref.list
		done	


#

#--- MAIN loop ---
CID=0
for CLASS in $CLASSES
do
    rm -f input.list
  echo "#>> CLASS= $CID"
  {
      for WID in $WIDS
	do
	echo $DATADIR/${WID}/${WID}${CLASS}.dat >> input.list
      done
  } 
  $RECOG ${PIXEL_WIDTH} ${PIXEL_WIDTH} input.list ref.list

  CID=`expr $CID + 1`
done
#

rm -f input.list ref.list
