#! /bin/sh
#

AILP_HOME="`dirname $0`/../"
PROGRAM="$AILP_HOME/bin/pixel_train.sh"		# training program file

#---- for upper case alphabet---
# KIND="u"	# character set:
# NCLASSES=26	# number of classes
#----- for digits --------------
KIND="d"	# character set:
NCLASSES=10	# number of classes

# size of pixel
PIXEL_WIDTH=64

# Directory of PIXEL data (input)
DATADIR=/group/teaching/ailp/DB/mit-pixels-${PIXEL_WIDTH}

# Directory of reference patterns (output)
ODIR="REF${PIXEL_WIDTH}-${KIND}${NCLASSES}-cv2-0"

# filename containing a list of writers used for training
LISTFILES="$AILP_HOME/CV-LISTS/list-cv2-0"

#---------------------------------------------------------------

# include the list of writers into the variable
WIDS=`cat ${LISTFILES} | tr "\n" " "`
# echo "WIDS=$WIDS"

# calculate the number of writers in the list, and assign it to the variable
NWRITERS=`echo $WIDS|wc -w| sed "s/[ \t]//g"`

echo "# number of writers = $NWRITERS"

#--- Create an output directory if it does not exist ---
if [ ! -d $ODIR ]; then
    echo "Non-existent output directory: $ODIR, creating one now"
    mkdir -p $ODIR
fi
#

#--- create a list of classes to train --- 
N=1
while [ $N -le $NCLASSES ]
do
    S=`printf "%02d" $N`
    CLASSES="$CLASSES ${KIND}$S"
    N=`expr $N + 1`
done
#

#--- Main loop ---
for CLASS in $CLASSES
  do
    rm -f input.list
  for WID in $WIDS
  do
    echo $DATADIR/$WID/${WID}${CLASS}.dat >> input.list
  done

  echo "# training class $CLASS"
  $PROGRAM ${PIXEL_WIDTH} ${PIXEL_WIDTH} input.list  > $ODIR/ref-$CLASS
done
#
rm -f input.list


