#!/usr/bin/ruby



$trainingset1=(51.75)
$traningset2=(76..99)

$datasetdir = "/home/cronin/Brogramming/AILP/ailp2/mit-points/" 
correct = 0
wrong = 0
resultmap = Hash.new
['u','d'].each do |realtype|
  chars = 26 if realtype == 'u'
  chars = 10 if realtype == 'd'
  (1..chars).each do |realchar|
    (1..50).each do |n| 
      samples = Hash.new
      ['u','d'].each do |t|
        chars = 26 if t == 'u'
        chars = 10 if t == 'd'
        (1..chars).each do |c|
          Thread.new{
            $trainingset1.each do |m|
               firstpath = "#{$datasetdir}s#{(n).to_s.rjust(3,'0')}/s#{(n).to_s.rjust(3,'0')}#{realtype}#{realchar.to_s.rjust(2,'0')}.dat"
               secondpath = "#{$datasetdir}s#{(m).to_s.rjust(3,'0')}/s#{(m).to_s.rjust(3,'0')}#{t}#{c.to_s.rjust(2,'0')}.dat"
               samples[[n,m,t,c]] = `./a.out #{firstpath} #{secondpath}`.to_f if (samples[[m+1,n+1,t,c]].nil? && m != n)
            end 
          }
          $trainingset2.each do |m|
               firstpath = "#{$datasetdir}s#{(n).to_s.rjust(3,'0')}/s#{(n).to_s.rjust(3,'0')}#{realtype}#{realchar.to_s.rjust(2,'0')}.dat"
               secondpath = "#{$datasetdir}s#{(m).to_s.rjust(3,'0')}/s#{(m).to_s.rjust(3,'0')}#{t}#{c.to_s.rjust(2,'0')}.dat"
               samples[[n,m,t,c]] = `./a.out #{firstpath} #{secondpath}`.to_f if (samples[[m+1,n+1,t,c]].nil? && m != n)
          end  
        end
      end
      reference = samples.sort{|x,y| x[1] <=> y[1]}[0]
      type = reference[0][2]
      char = reference[0][3]
      if type == realtype && char == realchar
        puts "Correct! #{reference}" 
        correct+=1
      else
        puts "Wrong! #{reference}"
        wrong+=1
      end 
      resultmap[[realchar,char,realtype,type]] = (resultmap[[realchar,char,realtype,type]] || 0)+1
      puts "#{resultmap}"
      puts "success rate: #{correct.to_f/(correct+wrong)}."
    end
  end
end
