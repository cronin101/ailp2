#!/usr/bin/ruby

$datasetdir = "../mit-points/" 

class Point
  @x
  @y
  attr_accessor :x, :y

  def initialize( x, y)
    @x=x
    @y=y
  end
end

class Points
  @list

  def initialize( list )
    @list=list
  end

  def points
    return @list
  end
end

def distance_between_points( p1, p2 )
  return Math.sqrt((p1.x - p2.x)**2 + (p1.y - p2.y)**2)
end

def compute_cost_matrix( set1, set2 )
  costmatrix = Hash.new
  set1.each do |s1p|
    set2.each do |s2p|
      costmatrix[[s1p,s2p]] = distance_between_points(s1p,s2p)
    end
  end
  return costmatrix
end

def compute_optimum_path( set1, set2, costmatrix )
  optimum = Hash.new
  set1.each{ |s1p| optimum[[s1p,0]] = 1000000**2 }
  set2.each{ |s2p| optimum[[0,s2p]] = 1000000**2 }
  optimum[[0,0]] = 0
  prev1 = 0
  set1.each do |s1p|
    prev2 = 0
    set2.each do |s2p|
       optimum[[s1p,s2p]] = costmatrix[[s1p,s2p]] + [optimum[[prev1,prev2]],optimum[[s1p,prev2]],optimum[[prev1,s2p]]].min
      prev2 = s2p
    end
    prev1 = s1p
  end
  return optimum[[set1.last,set2.last]]
end

def distance_between_pointsets( set1, set2 )
  costmatrix = compute_cost_matrix(set1,set2)
  return compute_optimum_path(set1,set2,costmatrix)
end

def extract_points_from_file( filename )
  points= []
  File.open(filename).each_line do |p|
    if p.split[2].to_i != 0
      point = Point.new(p.split[0].to_f, p.split[1].to_f)
      points << point
    end
  end
  pointset = Points.new(points)
  return pointset
end

