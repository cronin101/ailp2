#!/usr/bin/ruby

$trainingset1=(1..40)
$trainingset2=(41..80)

$datasetdir = "../mit-points/" 

firstpath = ""

#Sample number
n = ARGV[0]
#Class of sample
realtype = ARGV[1]
#Sample number/indicator
realchar = ARGV[2]

#Possible classes
if ARGV[3] == 'b'
  possible = ['u','d']
elsif ARGV[3] == 'd'
  possible = ['d']
elsif ARGV[3] == 'u'
  possible = ['u']
end

#Empty hash of samples before iteration
samples = Hash.new
#For each possible class of reference
possible.each do |t|
  #Uppercase characters have 26 variants
  chars = 26 if t == 'u'
  #Digit characters have 10 variants (0..9)
  chars = 10 if t == 'd'
  #Find offline distance for each reference pattern
  remaining = Hash.new
  Thread.new{
  (1..(chars/2).to_i).each do |c|
    remaining[c] = `./offline.o #{$datasetdir}s#{n.to_s.rjust(3,'0')}/s#{n.to_s.rjust(3,'0')}#{realtype}#{realchar.to_s.rjust(2,'0')}.dat ../References/#{t}#{c.to_s.rjust(2,'0')}.dat`
  end
  }
  ((chars/2).to_i..chars).each do |c|
    remaining[c] = `./offline.o #{$datasetdir}s#{n.to_s.rjust(3,'0')}/s#{n.to_s.rjust(3,'0')}#{realtype}#{realchar.to_s.rjust(2,'0')}.dat ../References/#{t}#{c.to_s.rjust(2,'0')}.dat`
 
    end

  besthalf= remaining.sort{|x,y| x[1] <=> y[1]}[0..(chars/2)-1]
  #For the characters that did best in the offline test
  besthalf.each do |c, v|
    #Generate half of the hashtable in a seperate thread. Data is concurrency safe since the hashtable keys are disjoint.
    Thread.new{
      #For the first half of the reference set
      $trainingset1.each do |m|
         firstpath = "#{$datasetdir}s#{(n).to_s.rjust(3,'0')}/s#{(n).to_s.rjust(3,'0')}#{realtype}#{realchar.to_s.rjust(2,'0')}.dat"
         secondpath = "#{$datasetdir}s#{(m).to_s.rjust(3,'0')}/s#{(m).to_s.rjust(3,'0')}#{t}#{c.to_s.rjust(2,'0')}.dat"
         #Store in the hash the distance of each reference sample from the supplied sample.
         samples[[n,m,t,c]] = `./a.out #{firstpath} #{secondpath}`.to_f if (samples[[m,n,t,c]].nil? && m != n)
      end 
    }
    #For the second half of the reference set
    $trainingset2.each do |m|
         firstpath = "#{$datasetdir}s#{(n).to_s.rjust(3,'0')}/s#{(n).to_s.rjust(3,'0')}#{realtype}#{realchar.to_s.rjust(2,'0')}.dat"
         secondpath = "#{$datasetdir}s#{(m).to_s.rjust(3,'0')}/s#{(m).to_s.rjust(3,'0')}#{t}#{c.to_s.rjust(2,'0')}.dat"
         #Store in the hash the distance of each reference sample from the supplied sample.
         samples[[n,m,t,c]] = `./a.out #{firstpath} #{secondpath}`.to_f if (samples[[m,n,t,c]].nil? && m != n)
    end  
  end
end
#Return the  sample that matches the closest with the sample.
reference = samples.sort{|x,y| x[1] <=> y[1]}[0]
type = reference[0][2]
char = reference[0][3]

#puts count.sort{|x,y| y[1] <=> x[1]}
 
puts "#{type} #{char}"
