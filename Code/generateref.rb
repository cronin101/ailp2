#!/usr/bin/ruby

require_relative 'handwriting'

$dspath = "../mit-points/"

$numpeople = 159


`rm -r ../References`
`mkdir ../References`

puts "Generating reference patterns..."
['u','d'].each do |type|
  chars = 10
  chars = 26 if type == 'u'
  (1..chars).each do |sample|
    pointmap = Hash.new
    (1..159).each do |person|
      pointset = extract_points_from_file("#{$dspath}s#{person.to_s.rjust(3,'0')}/s#{person.to_s.rjust(3,'0')}#{type}#{sample.to_s.rjust(2,'0')}.dat")
      unique = []
      pointset.points.each{|p| unique << [p.x,p.y]}
      unique.uniq!
      #s = 0.0
      #unique.each{|p| s= s+1}
      #xc = 0.0
      #unique.each{|p| xc = xc + p[0]}
      #xc = xc / s
      #yc = 0.0
      #unique.each{|p| yc = yc + p[1]}
      #yc = yc / s
      unique.each{|p| pointmap[[p[0],p[1]]] = (pointmap[[p[0],p[1]]] || 0)+1}
    end
File.open("../References/#{type}#{sample.to_s.rjust(2,'0')}.dat",'w'){|f| pointmap.each{|k,v| f.puts "#{k[0].to_i} #{k[1].to_i} #{v.to_f/$numpeople}"}}
  end
end
