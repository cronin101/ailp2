#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>

//Point definition
typedef struct{
  float x;
  float y;
  float t;
}point_t;

typedef struct{
  point_t *points;
  int num;
}pointset;

FILE *fr;

typedef int array[];

//Reads a set of points from file and saves in an array of point_t { x,y,time}
point_t* read_set_from_file (char* filename, int *size){
  int i,j;
  char line[80];
  char *str;
  char *token,*saveptr;
  point_t* points;
  j=0;
  points = malloc(sizeof(point_t));
  fr = fopen(filename,"rt");
  while(fgets(line,80,fr) != NULL){
    token = line;
    if(strlen(token) == 1){
      break;
    }
    j++;
    points = realloc(points,j*sizeof(point_t));
    i=0;
    for(str = line; ; str= NULL){
      if (i >= 3){
        break;
      }
      token = strtok_r(token," ",&saveptr);
      if (token == NULL){
        break;
      }
      switch (i){
        case (0): sscanf(token,"%f",&points[j-1].x); break;
        case (1): sscanf(token,"%f",&points[j-1].y); break;
        case (2): sscanf(token,"%f",&points[j-1].t); break;
      }
      i++;
      token = NULL;
    }
  }
  *size = j;
  return points;
}

//Calculates distance between two points
float distance_between_points( point_t p1, point_t p2 ){
  return sqrtf(pow(p1.x-p2.x,2)+pow(p1.y-p2.y,2));
}

//Returns the minimum of three inputted numbers
float min(float a, float b, float c){
  float m = a; 
  if (m > b) m = b;
  if (m > c) m = c;
  return m;
}

//Dynamic time warping algorithm to find the lowest distance between two sets of points regardless of timing scale
float compute_optimum_path( pointset seta, pointset setb){
  int i,j,cost;
  float optimum[seta.num+1][setb.num+1];
  for (i = 1; i <= seta.num; i++){
    optimum[i][0] = INT_MAX;
  }
  for (j = 1; j <= setb.num; j++){
    optimum[0][j] = INT_MAX;
  }
  optimum[0][0]=0;

  for (i = 1; i <= seta.num; i++){
    for (j = 1; j<= setb.num; j++){
      cost=distance_between_points(seta.points[i-1],setb.points[j-1]);
      optimum[i][j] = cost +min(optimum[i-1][j],optimum[i][j-1],optimum[i-1][j-1]);
    }
  }
  return optimum[seta.num][setb.num];
}

//Prints the contents of an array, used for debugging
void print_points (pointset list){
  int i;
  for (i = 0; i<list.num; i++){
    printf("%f",list.points[i].x);
  }
}

//Cuts short one point set to fit with another
void reducepoints( pointset *set , int nump){
  int i;
  set->points[nump-1] = set->points[set->num-1];
  set->num = nump;

}

//Used to find the total number of points in a set
float find_s(pointset set){
  int i;
  float s = 0;
  for (i = 0; i < set.num; i++){
    s+= 1;
  }
  return s;
}

//Translates points in a set to have a centroid of (0,0)
void translatepoints(pointset set, float xc, float yc){
  int i;
  for (i = 0; i < set.num; i++){
    set.points[i].x-= xc;
    set.points[i].y-= yc;
  }
}

//Finds the x centroid of a point set
float xcentroid(pointset set, float s){
  int i;
  float xc = 0;
  for (i = 0; i < set.num; i++){
    xc+= set.points[i].x;
  }
  return xc/s;
}

//Finds the y centroid of a point set
float ycentroid(pointset set, float s){
  int i;
  float yc = 0;
  for (i = 0; i < set.num; i++){
    yc+= set.points[i].y;
  }
  return yc/s;
}

//Main function, called by classification scripts
int main (int argc, char *argv[]){
  int *costmatrix = NULL;
  float distance,s;
  pointset set1,set2;
  set1.num = 0;
  //The first point set is read from the first commandline argument
  set1.points = read_set_from_file(argv[1],&set1.num);
  set2.num = 0;
  //The second point set is read from the second commandline argument
  set2.points = read_set_from_file(argv[2],&set2.num);
  //Both point sets are normalised to have a centroid of (0,0)
  s = find_s(set1);
  translatepoints(set1,xcentroid(set1,s),ycentroid(set1,s));
  s = find_s(set2);
  translatepoints(set2,xcentroid(set2,s),ycentroid(set2,s));
  //The distance between the two point sets is computed using dynamic time warping and then outputted
  distance =compute_optimum_path(set1, set2);
  printf("%f\n",distance);
  }
