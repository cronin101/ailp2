#!/usr/bin/ruby


$testingset = (81..159)

#if ARGV[0] == 'b'
#  possible = ['u','d']
if ARGV[0] == 'u'
  possible = ['u']
elsif ARGV[0] == 'd'
  possible = ['d']
end

correct = 0
wrong = 0
numsamples = ($testingset.max - $testingset.min) +1 
resultmap = Hash.new
$testingset.each do |s|
  possible.each do |t|
    chars = 26 if t == 'u'
    chars = 10 if t == 'd'
    (1..chars).each do |c|
      classification= `./bestmatches.rb #{s} #{t} #{c} #{ARGV[0]}` if ARGV[1] == 'b'
      classification= `./closestmatch.rb #{s} #{t} #{c} #{ARGV[0]}` if ARGV[1] == 'c'
      classification= `./hybridmatch.rb #{s} #{t} #{c} #{ARGV[0]}` if ARGV[1] == 'h'
      classification= `./offlinematch.rb #{s} #{t} #{c} #{ARGV[0]}` if ARGV[1] == 'o'
      cc = classification.split[1].to_s
      ct = classification.split[0].to_s
      if ct == t.to_s && cc == c.to_s
        correct+=1
      else
        wrong+=1
      end
      resultmap[[t.to_s,c.to_s,ct.to_s,cc.to_s]] = (resultmap[[t.to_s,c.to_s,ct.to_s,cc.to_s]] || 0) +1
      puts"Sample is [#{t},#{c}]. Classified as [#{ct},#{cc}]. #{correct.to_f*100/(correct+wrong)}% correct." if ARGV[2] == 'o'  
    end
  end
end
puts "Finished testing recognition algorithm on #{"Uppercase"if possible == ['u']}#{"Numerical"if possible == ['d']} characters..."
chars = 26 if ARGV[0] == 'u'
chars = 10 if ARGV[0] == 'd'
puts "#{numsamples*chars} samples tested, #{correct.to_f*100/(correct+wrong)}% correct."
puts "Confusion matrix is: "
print"     "
(1..chars).each do |num|
  print "#{num}     "
end
puts
(1..chars).each do |row|
    print "#{row.to_s.rjust(2,'0')} "
  (1..chars).each do |col|
    print "#{((resultmap[[possible[0].to_s,row.to_s,possible[0].to_s,col.to_s]]||0).to_f*100/numsamples).to_i.to_s.rjust(5,' ')[0..4]} "
  end
  puts 
end

