#!/usr/bin/ruby

$trainingset1=(81..120)
$trainingset2=(121..159)

$datasetdir = "../mit-points/" 

firstpath = ""

#Sample number
n = ARGV[0]
#Class of sample
realtype = ARGV[1]
#Sample number/indicator
realchar = ARGV[2]

#Possible classes
if ARGV[3] == 'b'
  possible = ['u','d']
elsif ARGV[3] == 'd'
  possible = ['d']
elsif ARGV[3] == 'u'
  possible = ['u']
end
chars = 10 if possible == ['d']
chars = 26 if possible == ['u']
t = possible[0]
#Find offline distance for each reference pattern
remaining = Hash.new
Thread.new{
(1..(chars/2).to_i).each do |c|
  remaining[[t,c]] = `./offline.o #{$datasetdir}s#{n.to_s.rjust(3,'0')}/s#{n.to_s.rjust(3,'0')}#{realtype}#{realchar.to_s.rjust(2,'0')}.dat ../References/#{t}#{c.to_s.rjust(2,'0')}.dat`
end
}
((chars/2).to_i..chars).each do |c|
  remaining[[t,c]] = `./offline.o #{$datasetdir}s#{n.to_s.rjust(3,'0')}/s#{n.to_s.rjust(3,'0')}#{realtype}#{realchar.to_s.rjust(2,'0')}.dat ../References/#{t}#{c.to_s.rjust(2,'0')}.dat`
end

best= remaining.sort{|x,y| x[1] <=> y[1]}[0]

#Return the  sample that matches the closest with the sample.
type = best[0][0]
char = best[0][1]

#puts count.sort{|x,y| y[1] <=> x[1]}
 
puts "#{type} #{char}"
