#!/usr/bin/ruby

$trainingset1=(81..120)
$trainingset2=(121..159)

$datasetdir = "../mit-points/" 

#Sample number
n = ARGV[0]
#Class of sample
realtype = ARGV[1]
#Sample number/indicator
realchar = ARGV[2]

#Possible classes
if ARGV[3] == 'b'
  possible = ['u','d']
elsif ARGV[3] == 'd'
  possible = ['d']
elsif ARGV[3] == 'u'
  possible = ['u']
end

#Empty hash of samples before iteration
samples = Hash.new
#For each possible class of reference
possible.each do |t|
  #Uppercase characters have 26 variants
  chars = 26 if t == 'u'
  #Digit characters have 10 variants (0..9)
  chars = 10 if t == 'd'
  #For each varient of character
  (1..chars).each do |c|
    #Generate half of the hashtable in a seperate thread. Data is concurrency safe since the hashtable keys are disjoint.
    Thread.new{
      #For the first half of the reference set
      $trainingset1.each do |m|
         firstpath = "#{$datasetdir}s#{(n).to_s.rjust(3,'0')}/s#{(n).to_s.rjust(3,'0')}#{realtype}#{realchar.to_s.rjust(2,'0')}.dat"
         secondpath = "#{$datasetdir}s#{(m).to_s.rjust(3,'0')}/s#{(m).to_s.rjust(3,'0')}#{t}#{c.to_s.rjust(2,'0')}.dat"
         #Store in the hash the distance of each reference sample from the supplied sample.
         samples[[n,m,t,c]] = `./a.out #{firstpath} #{secondpath}`.to_f if (samples[[m,n,t,c]].nil? && m != n)
      end 
    }
    #For the second half of the reference set
    $trainingset2.each do |m|
         firstpath = "#{$datasetdir}s#{(n).to_s.rjust(3,'0')}/s#{(n).to_s.rjust(3,'0')}#{realtype}#{realchar.to_s.rjust(2,'0')}.dat"
         secondpath = "#{$datasetdir}s#{(m).to_s.rjust(3,'0')}/s#{(m).to_s.rjust(3,'0')}#{t}#{c.to_s.rjust(2,'0')}.dat"
         #Store in the hash the distance of each reference sample from the supplied sample.
         samples[[n,m,t,c]] = `./a.out #{firstpath} #{secondpath}`.to_f if (samples[[m,n,t,c]].nil? && m != n)
    end  
  end
end
#Return the 10 reference samples that match the closest with the sample.
reference = samples.sort{|x,y| x[1] <=> y[1]}[0..9]
count = Hash.new
#Return the majority element or the first if there was none.
reference.each {|key, value| count[[key[2],key[3]]] = (count[[key[2],key[3]]] || 0) + 1}
classification = count.sort{|x,y| y[1] <=> x[1]}[0..1]
if classification.max != classification.min
  classification = count.sort{|x,y| y[1] <=> x[1]}[0]
  type = classification[0][0]
  char = classification[0][1]
else
  type = reference[0][0][2]
  char = reference[0][0][3]

end
#puts count.sort{|x,y| y[1] <=> x[1]}
 
puts "#{type} #{char}"
